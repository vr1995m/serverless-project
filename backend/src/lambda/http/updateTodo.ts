import 'source-map-support/register'
import * as AWS  from 'aws-sdk'
import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult } from 'aws-lambda'

import { UpdateTodoRequest } from '../../requests/UpdateTodoRequest'

const docClient = new AWS.DynamoDB.DocumentClient()
const todoTable = process.env.TODO_TABLE

export const handler: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const todoId = event.pathParameters.todoId
  const updatedTodo: UpdateTodoRequest = JSON.parse(event.body)

  const params = {
    TableName: todoTable,
    Key: {
      "id": todoId,
    },
    UpdateExpression: "set name = :n, dueDate= :due, done: :d",
    ExpressionAttributeValues: {
      ":n": updatedTodo['name'],
      ":due":  updatedTodo.dueDate,
      ":d": updatedTodo.done
    },
    ReturnValues: "UPDATED_NEW"
  };


  try {
    const data = await docClient.update(params).promise();
    return {
      'statusCode': 200,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        data,
        message: 'Todo updated!'
      })
    }
    } catch (err) {
    return {
      'statusCode': 400,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        error: err,
        message: 'Todo not updated!'
      })
    }
  }
}
