import 'source-map-support/register'
import * as uuid from 'uuid'
import * as AWS  from 'aws-sdk'
import { parseUserId } from '../../auth/utils'

import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult } from 'aws-lambda'

import { CreateTodoRequest } from '../../requests/CreateTodoRequest'

const docClient = new AWS.DynamoDB.DocumentClient()
const todoTable = process.env.TODO_TABLE

export const handler: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const newTodo: CreateTodoRequest = JSON.parse(event.body)

  console.log('Processing event: ', event)
  const todoId = uuid.v4()

  const authorization = event.headers.Authorization
  const split = authorization.split(' ')
  const jwtToken = split[1]

  const todo = {
    id: todoId,
    userId: parseUserId(jwtToken),
    ...newTodo
  }
  
  const params = {TableName: todoTable, Item: todo}

  try {
    const data = await docClient.put(params).promise();
    return {
      'statusCode': 200,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        data,
        message: 'Todo created!'
      })
    }
    } catch (err) {
    return {
      'statusCode': 400,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        error: err,
        message: 'Todo not exists!'
      })
    }
  }
}
