import 'source-map-support/register'
import * as AWS  from 'aws-sdk'
import { parseUserId } from '../../auth/utils'
import { APIGatewayProxyEvent, APIGatewayProxyResult, APIGatewayProxyHandler } from 'aws-lambda'

const docClient = new AWS.DynamoDB.DocumentClient()
const todoTable = process.env.TODO_TABLE
const todoIdIndex = process.env.TODO_ID_INDEX

export const handler: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  console.log('Processing event: ', event)

  const authorization = event.headers.Authorization
  const split = authorization.split(' ')
  const jwtToken = split[1]

  const userId = parseUserId(jwtToken)

  const params = {
    TableName : todoTable,
    IndexName : todoIdIndex,
    KeyConditionExpression: 'userId = :userId',
    ExpressionAttributeValues: {
        ':userId': userId
    }
  }

  try {
    const data = await docClient.query(params).promise();
    const todo = data.Items
    return {
      'statusCode': 200,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        todo,
        message: 'Todo created'
      })
    }
    } catch (err) {
    return {
      'statusCode': 400,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        error: err,
        message: 'Todo not exists!'
      })
    }
  }
}
