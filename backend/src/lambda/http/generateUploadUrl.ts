import 'source-map-support/register'
import * as uuid from 'uuid'
import * as AWS  from 'aws-sdk'

import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult } from 'aws-lambda'

const docClient = new AWS.DynamoDB.DocumentClient()
const todoTable = process.env.TODO_TABLE


export const handler: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const todoId = event.pathParameters.todoId

  console.log('Processing event: ', event, todoId)
  const urlId = uuid.v4();
  var s3 = new AWS.S3();

  // Bucket params
  const bucket = process.env.S3_BUCKET
  const urlExpires = process.env.URL_EXPIRATION
  var params = { Bucket: bucket, Key: urlId, Expires: urlExpires };

  const signedUrl = s3.getSignedUrl('putObject', params);

  const imageUrl = `https://${bucket}.s3.amazonaws.com/${urlId}`;

  const updateUrlTodo = {
    TableName: todoTable,
    Key: { 
      "id": todoId 
    },
    UpdateExpression: "set attachUrl = :a",
    ExpressionAttributeValues:{
      ":a": imageUrl
    },
    ReturnValues:"UPDATED_NEW"
  };

  try {
    const data = await docClient.update(updateUrlTodo).promise();
    const todo = data.$response
    return {
      'statusCode': 200,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        todo,
        imageUrl: imageUrl,
        uploadUrl: signedUrl
      })
    }
    } catch (err) {
    return {
      'statusCode': 400,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      'body': JSON.stringify({
        error: err,
        message: 'Todo not updated!'
      })
    }
  }
}
